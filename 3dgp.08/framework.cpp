#include "framework.h"
#include "iostream"
#include "Blend.h"//追加
using namespace std;

bool framework::initialize() {
	//---------------------------------------
	//01追加----------------------------------
	//---------------------------------------

	HRESULT hr = S_OK;
	UINT createDeviceFlag = 0;
#ifdef _DEBUG
	createDeviceFlag |= D3D11_CREATE_DEVICE_DEBUG;
#elif
	createDeviceFlag = D3D11_CREATE_DEVICE_SINGLETHREADED;
#endif
	D3D_DRIVER_TYPE driverTypes[] = {
		//D3D_DRIVER_TYPE_UNKNOWN,
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_REFERENCE,
		D3D_DRIVER_TYPE_WARP,
	};
	UINT numdriverTypes = ARRAYSIZE(driverTypes);//featurelevelsの配列数
	D3D_FEATURE_LEVEL featureLevels[] =
	{
	    D3D_FEATURE_LEVEL_11_0,//DirectX11のみ 9.x以降は無視
	    D3D_FEATURE_LEVEL_10_1,
	    D3D_FEATURE_LEVEL_10_0,
	};
	UINT numFeatureLevel = ARRAYSIZE(featureLevels);//featurelevelsの配列数

#ifdef CREATE_DEVICE_AND_SWAPCHAIN
    //スワップ チェーンを記述します。
	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	ZeroMemory(&swapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	swapChainDesc.BufferDesc.Width = SCREEN_WIDTH;
	swapChainDesc.BufferDesc.Height = SCREEN_HEIGHT;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	swapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.BufferCount = 1;
	swapChainDesc.OutputWindow = hwnd;
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.SampleDesc.Quality = 0;
	swapChainDesc.Windowed = TRUE;

	//ディスプレイ アダプターを表すデバイスとレンダリングに使用するスワップ チェーンを作成します。
	//UINT driverTypeIndex = 1;
	for (UINT driverTypeIndex = 0; driverTypeIndex < numdriverTypes; driverTypeIndex++) {
		D3D_DRIVER_TYPE drvType = driverTypes[driverTypeIndex];
		D3D_FEATURE_LEVEL featuLevel;
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			drvType,
			NULL,
			createDeviceFlag,
			featureLevels,
			numFeatureLevel,
			D3D11_SDK_VERSION,
			&swapChainDesc,
			&p_SwapChain,
			&p_Device,
			&featuLevel,
			&p_DeviceContext
		);
		if (SUCCEEDED(hr))break;
		cout << driverTypeIndex;
	}

	_ASSERT_EXPR_A(SUCCEEDED(hr), hr_trace(hr));
	if (FAILED(hr))return false;

#else //CREATE_DEVICE_AND_SWAPCHAIN
	for (UINT driverTypeIndex = 0; driverTypeIndex < numdriverTypes; driverTypeIndex++) {
		D3D_DRIVER_TYPE drvType = driverTypes[driverTypeIndex];
		D3D_FEATURE_LEVEL featuLevel;
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			drvType,
			NULL,
			createDeviceFlag,
			featureLevels,
			numFeatureLevel,
			D3D11_SDK_VERSION,
			&p_SwapChain,
			&p_Device,
			&featuLevel,
			&p_DeviceContext
		);
		if (SUCCEEDED(hr))break;
	}
	_ASSERT_EXPR_A(SUCCEEDED(hr), hr_trace(hr));
	if (FAILED(hr))return false;
	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));
	swapChainDesc.BufferDesc.Width = SCREEN_WIDTH;
	swapChainDesc.BufferDesc.Height = SCREEN_HEIGHT;
	swapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.BufferCount = 1;
	swapChainDesc.OutputWindow = hwnd;
#endif //CREATE_DEVICE_AND_SWAPCHAIN
	//CreateRenderTargetView(リソース データへのアクセス用にレンダー ターゲット ビューを作成します。)
	ID3D11Texture2D* pBuckbuffer = NULL;
	hr = p_SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBuckbuffer);
	_ASSERT_EXPR_A(SUCCEEDED(hr), hr_trace(hr));
	if (FAILED(hr))return false;
	hr = p_Device->CreateRenderTargetView(pBuckbuffer, NULL, &p_RenderTargetView);
	pBuckbuffer->Release();
	_ASSERT_EXPR_A(SUCCEEDED(hr), hr_trace(hr));
	if (FAILED(hr))return false;
	//CreateTexture2D(2D テクスチャーの配列を作成します。)
	ID3D11Texture2D* DepthStencil;
	D3D11_TEXTURE2D_DESC tex2Ddesk;//2D画面分割のデータ
	ZeroMemory(&tex2Ddesk, sizeof(tex2Ddesk));
	tex2Ddesk.Width = SCREEN_WIDTH;
	tex2Ddesk.Height = SCREEN_HEIGHT;
	tex2Ddesk.MipLevels = 1;
	tex2Ddesk.ArraySize = 1;
	tex2Ddesk.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	tex2Ddesk.SampleDesc.Count = 1;
	tex2Ddesk.SampleDesc.Quality = 0;
	tex2Ddesk.Usage = D3D11_USAGE_DEFAULT;
	tex2Ddesk.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	tex2Ddesk.CPUAccessFlags = 0;
	tex2Ddesk.MiscFlags = 0;
	hr = p_Device->CreateTexture2D(&tex2Ddesk, NULL, &DepthStencil);//2D画面の配列の作成
	_ASSERT_EXPR_A(SUCCEEDED(hr), hr_trace(hr));
	if (FAILED(hr))return false;

	//CreateDepthStencilView//深度ステンシルビュー
	D3D11_DEPTH_STENCIL_VIEW_DESC descDepthStencil;
	ZeroMemory(&descDepthStencil, sizeof(descDepthStencil));
	descDepthStencil.Format = tex2Ddesk.Format;
	descDepthStencil.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDepthStencil.Texture2D.MipSlice = 0;
	hr = p_Device->CreateDepthStencilView(DepthStencil, &descDepthStencil, &p_DepthStencilView);
	DepthStencil->Release();
	_ASSERT_EXPR_A(SUCCEEDED(hr), hr_trace(hr));
	if (FAILED(hr))return false;
	////////////////////////////////////////
	//Added by Unit7
	///////////////////////////////////////
     //   BlendMode::Initializer(p_Device);
     //   // ブレンディング ステートを記述します。(1度設定すれば永続するのでframework.cppで作成)
     //   D3D11_BLEND_DESC BlendDesk;
     //   //D3D11_RENDER_TARGET_BLEND_DESC *1;
     //   ZeroMemory(&BlendDesk, sizeof(BlendDesk));
     //   //BlendDesk.AlphaToCoverageEnable = false;//ピクセルをレンダー ターゲットに設定するときに、アルファトゥカバレッジをマルチサンプリング テクニックとして使用するかどうかを決定します。
     //   //BlendDesk.IndependentBlendEnable = false;//同時処理のレンダー ターゲットで独立したブレンディングを有効にするには、TRUE に設定します。FALSE に設定すると、RenderTarget[0] のメンバーのみが使用されます。RenderTarget[1..7] は無視されます。
	    //				     //レンダリング ターゲットのブレンディング ステートを記述します。
	    //				     //D3D11_RENDER_TARGET_BLEND_DESC RenderTargetDesk[8];
	    //				     //ZeroMemory(&RenderTargetDesk, sizeof(RenderTargetDesk));
     //   //BlendDesk.RenderTarget[0].BlendEnable = true;
     //   //BlendDesk.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
     //   //BlendDesk.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
     //   //BlendDesk.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;//RGB またはアルファのブレンディング処理です。
     //   //BlendDesk.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
     //   //BlendDesk.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
     //   //BlendDesk.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
     //   //BlendDesk.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
     //   //BlendDesk.RenderTarget[8] = RenderTargetDesk;//レンダリング ターゲットのブレンディング ステートを記述します。
     //   //RenderTargetDesk.RenderTargetWriteMask;
     //   hr = p_Device->CreateBlendState(&BlendDesk, &p_BlendState);	//
     //   if (FAILED(hr)) {
	    //return false;
     //   }
     //   float blendFactor[4] = { D3D11_BLEND_ZERO, D3D11_BLEND_ZERO, D3D11_BLEND_ZERO, D3D11_BLEND_ZERO };
     //   //p_Device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
     //   p_DeviceContext->OMSetBlendState(p_BlendState, blendFactor, 0xffffffff);//出力結合ステージの深度ステンシル ステートを設定します。
	// 自作スプライトクラス-------------
	//////////////////////////////////////
	//Custamized by UNIT4
	//Deleted by UNIT5
	//////////////////////////////////////
	//sprites[0] = new sprite3D(p_Device);
	//sprites[1] = new sprite3D(p_Device, _T("player-sprites.png"));
	//////////////////////////////////////
	//Custamized by UNIT5
	//Deleted by Unit7
	//////////////////////////////////////
	/*for (auto &p : sprites) {
	    p = new sprite3D(p_Device, _T("player-sprites.png"));
	}*/
	/////////////////////////////////////
	////Added by Unit7
	/////////////////////////////////////
	BlendMode::Initializer(p_Device);
	/////////////////////////////////////
	////Custumed by Unit7
	/////////////////////////////////////
	sprites[0] = new Sprite3D(p_Device, L"logos.jpg");	//1920*1080
	sprites[1] = new Sprite3D(p_Device, L"n64.png");	//900*877
	////////////////////////////////////////
	//Added by Unit8
	///////////////////////////////////////
	particle = new Sprite3D(p_Device, L"particle-smoke.png");
	Font::LoadImage(p_Device, L"fonts\\font6.png");
	Font::SetColor(1, 1, 1, 1);
	//追加
	flameCounter = 0;//ベンチマーク用
	benchmarkTime = new float[BENCH_NFLAME];
	for (int i = 0; i < BENCH_NFLAME; i++)
	{
		benchmarkTime[i] = 0;
	}
	//---------------------------
	return true;
}
void framework::update(float elapsed_time/*Elapsed seconds from last frame*/) {
	//LARGE_INTEGER union;
	//QueryPerformanceFrequency(&);
	//QueryPerformanceCounter(&);
}
void framework::render(float elapsed_time/*Elapsed seconds from last frame*/) {

	// viewport(ビューポートの寸法を定義します。)ビューポートは -1.0〜1.0 の範囲で作られたワールド座標をスクリーン座標（表示するウインドウのサイズ）に変換するための情報)
	D3D11_VIEWPORT viewport;
	viewport.Width = (FLOAT)SCREEN_WIDTH;
	viewport.Height = (FLOAT)SCREEN_HEIGHT;//1lo0O10O0O8sSBloO
	viewport.MaxDepth = 1.0f;
	viewport.MinDepth = 0.0f;
	viewport.TopLeftX = 0;
	viewport.TopLeftY = 0;
	p_DeviceContext->RSSetViewports(1, &viewport);

	//レンダー ターゲットのすべての要素に 1 つの値を設定します。
	float ClearColor[4] = { 0.0f,0.125f,0.3f,1.0f };//red,green,blue,alpha
	p_DeviceContext->ClearRenderTargetView(p_RenderTargetView, ClearColor);
	//深度ステンシル リソースをクリアします。
	p_DeviceContext->ClearDepthStencilView(p_DepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	//1 つ以上のレンダー ターゲットをアトミックにバインドし、出力結合ステージに深度ステンシル バッファーをバインドします。
	p_DeviceContext->OMSetRenderTargets(1, &p_RenderTargetView, p_DepthStencilView);
	/////////////////////////////////////
	////Custumed by Unit7
	/////////////////////////////////////
	// 自作スプライトクラス----
	//sprites[0]->render(p_DeviceContext);
       /* static float r2 = 0;
	static float r = 0;
	r2 -= 0.1f;
	r += 0.1f;
	DirectX::XMFLOAT3 ver[4];
	ver[0] = DirectX::XMFLOAT3(-50, 50, .0f);
	ver[1] = DirectX::XMFLOAT3(50, 40, .0f);
	ver[2] = DirectX::XMFLOAT3(-20, -60, .0f);
	ver[3] = DirectX::XMFLOAT3(30, -20, .0f);*/
	/*sprites[0]->render(p_DeviceContext, SCREEN_W / 2, SCREEN_H / 2, 0.1f, 0.1f, 100, 100, 50, 50, r, 0, 0.5f, 0.2f, 1);
	sprites[0]->render(p_DeviceContext, SCREEN_W / 2, SCREEN_H / 2, 1.5f, 1.5f, 100, 100, 50, 50, r, 0.4f, 0.5f, 0.6f, 1);
	sprites[0]->render(p_DeviceContext, SCREEN_W/2, SCREEN_H/2, 2.5f, 2.5f, 100, 300, 150, 150, r, 1.0f, 0.5f, 0.6f, 1);*/
	//////////////////////////////////////
	//Deleted by UNIT4
	//////////////////////////////////////
     //   sprites[0]->render(p_DeviceContext,
	    //ver,//頂点位置
	    //(float)100, (float)100,//平行移動位置
	    //(float)1, (float)1,
	    ////float w, float h,//dw,dh;Size of sprite in screen space
	    //(float)0, (float)0,
	    //r,//angle:Raotation angle(Rotation centre is sprite's each vertices)
	    //(float)1, (float)1, (float)1, (float)1);
	//////////////////////////////////////
	//Added by UNIT4
	//////////////////////////////////////
     //   sprites[1]->render2(p_DeviceContext,
	    //200, 200,   //平行移動
	    ////140, 240,
	    //0, 0,//画像切り抜き
	    //140, 240,//画像切り抜き幅
	    //1, 1,//拡大
	    //0, 0,//中心
	    //r,//angle:Raotation angle(Rotation centre is sprite's each vertices)
	    //1, 1, 1, 1);
	//////////////////////////////////////
	//Added by UNIT5
	//////////////////////////////////////

     //   static float angle = 0;
     //   angle += 6.0f * elapsed_time;//1 round_per 60 seconds
     //   float x = 0;
     //   float y = 0;
     //   for (auto &p : sprites) {
	    //p->Render2(p_DeviceContext, x, float(static_cast<int>(y) % 720), 0, 0, 140, 240, 0.5f,0.5f, 0, 0, angle, 0, 1, 1, 1);
	    //x += 32;
	    //if (x > 1280 - 64) {
	    //    x = 0;
	    //    y += 24;
	    //}
     //   }



	static BlendMode::BLEND_MODE mode = BlendMode::ALPHA;
	if (GetAsyncKeyState('O') & 0x01)mode = (BlendMode::BLEND_MODE)((mode + (BlendMode::MODE_MAX - 1)) % BlendMode::MODE_MAX);
	if (GetAsyncKeyState('P') & 0x01)mode = (BlendMode::BLEND_MODE)((mode + 1) % BlendMode::MODE_MAX);
	static float alpha = 0.0f;
	if (GetAsyncKeyState('K') < 0)alpha = (alpha -= 0.005f) < 0.0f ? 0.0f : alpha;
	if (GetAsyncKeyState('L') < 0)alpha = (alpha += 0.005f) > 1.0f ? 1.0f : alpha;
	//alpha = 1;
	//mode = BlendMode::ALPHA;
	//this->mode = mode;
	//this->alpha = alpha;
	BlendMode::Set(p_DeviceContext, BlendMode::ALPHA);
	sprites[0]->Render2(p_DeviceContext, 0, 0, 0, 0, 960, 540, 1, 1, 480, 270, 0, 1, 1, 1, 0.5f);
	BlendMode::Set(p_DeviceContext, mode);
	sprites[1]->Render2(p_DeviceContext, 0, 0, 0, 0, 900, 877, 1, 1, 450, 438, 0, 1, 1, 1, alpha);
	/////////////////////////////////////
	////Added by Unit8
	/////////////////////////////////////

	static DirectX::XMFLOAT2 sprite_positon[1024] = {};//screen space
	static float timer = 4;//0-4sec
	//elapsed_timeは１フレームにかかった時間
	timer += elapsed_time;
	static float angle = 0;
	if (timer > 4.0f) {
		for (auto &p : sprite_positon) {//update positions once in four seconds
			float a = ((float)rand()) / RAND_MAX*360.0f;//angle(degree)
			float r = ((float)rand()) / RAND_MAX*256.0f;//radius(screen space)
			p.x = cosf(a*0.01745f)*r;
			p.y = sinf(a*0.01745f)*r;
		}
		timer = 0;
	}
	BlendMode::Set(p_DeviceContext, BlendMode::ADD);

	//ベンチマーク計測(1f)
	bm.begin();
	for (auto &p : sprite_positon) {
		particle->Render2(p_DeviceContext, p.x + 256, p.y + 256, 0, 0, 420, 420, 1, 1, 210, 210, angle * 4, 0.2f, 0.05f*timer, 0.01f*timer, fabsf(sinf(PI*timer*0.5f*0.5f)));
	}
	//ベンチマーク計測終了(1f)
	benchmarkTime[flameCounter] = bm.end()*1000.0f;//msなので1000倍

	//benchmarkTime[]の中で1晩時間が短いの
	float benchmarkMinTime = benchmarkTime[0];
	for (int i = 0; i < BENCH_NFLAME; i++)
	{
		float benchTime = benchmarkTime[i];
		//上書き条件
		if (benchmarkMinTime > benchTime) {
			benchmarkMinTime = benchTime;
		}
	}
	//キューやリングバッファ
	char msg[64];
	sprintf_s(msg, 64, "BENCH_MARK: %.3fms", benchmarkMinTime);
	Font::RenderText(p_DeviceContext, msg, 100, 20, 32, 32, 0);

	flameCounter++;//1f++
	if (flameCounter >= BENCH_NFLAME)flameCounter = 0;
	//---------------------

	//レンダリングされたイメージをユーザーに表示します。
	p_SwapChain->Present(0, 0);
	return;
}

