#pragma once
#include "sprite3D.h"

class Font {
private:
    static const int NUM_1LINE = 16;//�摜�̂P��ɕ���ł��镶����
    static Sprite3D* img;
    static float r, g, b, a;
    Font();
    ~Font();
public:
    static bool LoadImage(ID3D11Device* p_Device, const wchar_t* filename) {
	ReleaseImage();
	img = new Sprite3D(p_Device, filename);
	return true;
    }
    static void ReleaseImage() {
	if (img) {
	    delete img;
	    img = nullptr;
	}
    }
    static void SetColor(float r, float g, float b, float a) {
	Font::r = r;
	Font::g = g;
	Font::b = b;
	Font::a = a;
    }
    static void RenderText(ID3D11DeviceContext*,
	const char*,
	float x, float y, float h, float w,
	float angle
    );
};
