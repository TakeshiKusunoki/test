#include "Blend.h"//追加

ID3D11BlendState* BlendMode::BlendState[BlendMode::MODE_MAX];
bool BlendMode::bLoad = false;
BlendMode::BLEND_MODE BlendMode::enumMode;
//typedef enum D3D11_BLEND
//{
//    D3D11_BLEND_ZERO = 1,			//データ ソースの色は黒 (0, 0, 0, 0) です。
//    D3D11_BLEND_ONE = 2,			//データ ソースの色は白 (1, 1, 1, 1) です
//    D3D11_BLEND_SRC_COLOR = 3,		//データ ソースは、ピクセル シェーダーからのカラー データ (RGB) です。
//    D3D11_BLEND_INV_SRC_COLOR = 4,	//データ ソースは、ピクセル シェーダーからのカラー データ (RGB) です。ブレンディング前の処理によってデータが反転され、。
//    D3D11_BLEND_SRC_ALPHA = 5,		//データ ソースは、ピクセル シェーダーからのアルファ データ (A) です。
//    D3D11_BLEND_INV_SRC_ALPHA = 6,	//データ ソースは、レンダー ターゲットからのアルファ データです。ブレンディング前の処理によってデータが反転され、1 - A が生成されます。
//    D3D11_BLEND_DEST_ALPHA = 7,		//データ ソースは、レンダー ターゲットからのアルファ データです。
//    D3D11_BLEND_INV_DEST_ALPHA = 8,	//データ ソースは、レンダー ターゲットからのアルファ データです。ブレンディング前の処理によってデータが反転され、1 - A が生成されます。
//    D3D11_BLEND_DEST_COLOR = 9,		//データ ソースは、レンダー ターゲットからのカラー データです。
//    D3D11_BLEND_INV_DEST_COLOR = 10,	//データ ソースは、レンダー ターゲットからのカラー データです。ブレンディング前の処理によってデータが反転され、1 - RGB が生成されます。
//    D3D11_BLEND_SRC_ALPHA_SAT = 11,	//データ ソースは、ピクセル シェーダーからのアルファ データです。ブレンディング前の処理によってデータが 1 以下にクランプされます。
//    D3D11_BLEND_BLEND_FACTOR = 14,	//データ ソースは、ID3D11DeviceContext::OMSetBlendState で設定されたブレンディング係数です。ブ
//    D3D11_BLEND_INV_BLEND_FACTOR = 15,	//データ ソースは、ID3D11DeviceContext::OMSetBlendState で設定されたブレンディング係数です。ブレンディング前の処理によってブレンディング係数が反転され、1 - blend_factor が生成されます。
//    D3D11_BLEND_SRC1_COLOR = 16,	     /	//データ ソースは、ピクセル シェーダーによって出力された両方のカラー データです。ブレンディング前の処理はありません。このオプションは、デュアル ソースのカラー ブレンディングをサポートします。
//    D3D11_BLEND_INV_SRC1_COLOR = 17,	//データ ソースは、ピクセル シェーダーによって出力された両方のカラー データです。ブレンディング前の処理によってデータが反転され、1 - RGB が生成されます。このオプションは、デュアル ソースのカラー ブレンディングをサポートします。
//    D3D11_BLEND_SRC1_ALPHA = 18,		//データ ソースは、ピクセル シェーダーによって出力されたアルファ データです。ブレンディング前の処理はありません。このオプションは、デュアル ソースのカラー ブレンディングをサポートします。
//    D3D11_BLEND_INV_SRC1_ALPHA = 19,	//データ ソースは、ピクセル シェーダーによって出力されたアルファ データです。ブレンディング前の処理によってデータが反転され、1 - A が生成されます。このオプションは、デュアル ソースのカラー ブレンディングをサポートします。
//} D3D11_BLEND;
struct BLEND_DATA {
    D3D11_BLEND	    SrcBlend;		//Comentary・・・最初の RGB データ ソースを指定します
    D3D11_BLEND	    DestBlend;		//Comentary・・・2 番目の RGB データ ソースを指定します。
    D3D11_BLEND_OP  BlendOp;		//Comentary・・・RGB データ ソースの組合せ方法を定義します。
    D3D11_BLEND	    SrcBlendAlpha;	//Comentary・・・最初のアルファ データ ソースを指定します。
    D3D11_BLEND	    DestBlendAlpha;	//Comentary・・・2 番目のアルファ データ ソースを指定します。
    D3D11_BLEND_OP  BlendOpAlpha;		//Comentary・・・アルファ データ ソースの組合せ方法を定義します。
};
BLEND_DATA BlendData[BlendMode::MODE_MAX] =
{
    {//NONE,
	D3D11_BLEND_ONE,			//SrcBlend;
	D3D11_BLEND_ZERO,			//DestBlend;
	D3D11_BLEND_OP_ADD,		//BlendOp;
	D3D11_BLEND_ONE,			//SrcBlendAlpha;
	D3D11_BLEND_ZERO,			//DestBlendAlpha
	D3D11_BLEND_OP_ADD,		//BlendOpAlpha;
    },
    {//ALPHA,
	D3D11_BLEND_SRC_ALPHA,      //SrcBlend;
	D3D11_BLEND_ONE,            //DestBlend;
	D3D11_BLEND_OP_ADD,         //BlendOp;
	D3D11_BLEND_ONE,            //SrcBlendAlpha;
	D3D11_BLEND_ZERO,           //BlendOpAlpha;
	D3D11_BLEND_OP_ADD,         //
	// res.r = dst.r * (1 - src.a) + src.r * src.a
	// res.g = dst.g * (1 - src.a) + src.g * src.a
	// res.b = dst.b * (1 - src.a) + src.b * src.a
	// res.a = dst.a * (1 - src.a) + src.a

    },
    {//ADD,
	    D3D11_BLEND_SRC_COLOR,      //SrcBlend;
	D3D11_BLEND_ONE,            //DestBlend;
	D3D11_BLEND_OP_ADD,         //BlendOp;
	D3D11_BLEND_ONE,            //SrcBlendAlpha;
	D3D11_BLEND_ZERO,           //BlendOpAlpha;
	D3D11_BLEND_OP_ADD,         //
    },
    {//SUBTRACT,
	D3D11_BLEND_SRC_ALPHA,      //SrcBlend;
	D3D11_BLEND_ONE,     //DestBlend;
	D3D11_BLEND_OP_ADD,   //BlendOp;
	D3D11_BLEND_ONE,      //SrcBlendAlpha;
	D3D11_BLEND_ZERO,     //BlendOpAlpha;
	D3D11_BLEND_OP_ADD,   //
    },
    {//REPLACE,
	D3D11_BLEND_SRC_ALPHA,      //SrcBlend;
	D3D11_BLEND_ONE,     //DestBlend;
	D3D11_BLEND_OP_ADD,   //BlendOp;
	D3D11_BLEND_ONE,      //SrcBlendAlpha;
	D3D11_BLEND_ZERO,     //BlendOpAlpha;
	D3D11_BLEND_OP_ADD,   //
    },
    {//MULTIPLY,
	D3D11_BLEND_SRC_ALPHA,      //SrcBlend;
	D3D11_BLEND_ONE,            //DestBlend;
	D3D11_BLEND_OP_ADD,         //BlendOp;
	D3D11_BLEND_ONE,            //SrcBlendAlpha;
	D3D11_BLEND_ZERO,           //BlendOpAlpha;
	D3D11_BLEND_OP_ADD,         //
    },
    {//LIGHTEN,
	D3D11_BLEND_SRC_ALPHA,      //SrcBlend;
	D3D11_BLEND_ONE,     //DestBlend;
	D3D11_BLEND_OP_ADD,   //BlendOp;
	D3D11_BLEND_ONE,      //SrcBlendAlpha;
	D3D11_BLEND_ZERO,     //BlendOpAlpha;
	D3D11_BLEND_OP_ADD,   //
    },
    {//DARKEN,
	D3D11_BLEND_SRC_ALPHA,      //SrcBlend;
	D3D11_BLEND_ONE,     //DestBlend;
	D3D11_BLEND_OP_ADD,   //BlendOp;
	D3D11_BLEND_ONE,      //SrcBlendAlpha;
	D3D11_BLEND_ZERO,     //BlendOpAlpha;
	D3D11_BLEND_OP_ADD,   //
    },
    {//SCREEN,
	D3D11_BLEND_SRC_ALPHA,      //SrcBlend;
	D3D11_BLEND_ONE,     //DestBlend;
	D3D11_BLEND_OP_ADD,   //BlendOp;
	D3D11_BLEND_ONE,      //SrcBlendAlpha;
	D3D11_BLEND_ZERO,     //BlendOpAlpha;
	D3D11_BLEND_OP_ADD,   //
    },
};

//
//void BlendMode::BlendSwitch(D3D11_BLEND_OP mode) {
//    switch (mode) {
//    case D3D11_BLEND_OP_ADD:
//	BlendAdd();
//	break;
//    case D3D11_BLEND_OP_SUBTRACT:
//	BlendSubtract();
//	break;
//    case D3D11_BLEND_OP_REV_SUBTRACT:
//	BlendReplace();
//    case D3D11_BLEND_OP_MIN:
//	BlendMultiply(D3D11_BLEND mode1, D3D11_BLEND mode2);
//	BlendLighten();
//	BlendDarken();
//	BlendScreen();
//    default:
//	BlendAlpha();
//	break;
//    }
//}
//
//void BlendMode::BlendAlpha() {
//
//}
//void BlendMode::BlendAdd() {
//
//}
//void BlendMode::BlendSubtract() {
//
//}
//void BlendMode::BlendReplace() {
//
//}
//void BlendMode::BlendMultiply(D3D11_BLEND mode1, D3D11_BLEND mode2) {
//
//}
//void BlendMode::BlendLighten() {
//
//}
//void BlendMode::BlendDarken() {
//
//}
//void BlendMode::BlendScreen() {
//
//}

bool BlendMode::Initializer(ID3D11Device* p_Device) {
    HRESULT hr;
    D3D11_BLEND_DESC BlendDesc;
    ZeroMemory(&BlendDesc, sizeof(BlendDesc));
    for (BLEND_MODE mode = NONE; mode < MODE_MAX; mode = (BLEND_MODE)(mode + 1))
    {
	BlendDesc.AlphaToCoverageEnable = FALSE;
	BlendDesc.IndependentBlendEnable = FALSE;
	BlendDesc.RenderTarget[0].BlendEnable = TRUE;
	BlendDesc.RenderTarget[0].SrcBlend = BlendData[mode].SrcBlend;
	BlendDesc.RenderTarget[0].DestBlend = BlendData[mode].DestBlend;
	BlendDesc.RenderTarget[0].BlendOp = BlendData[mode].BlendOp;
	BlendDesc.RenderTarget[0].SrcBlendAlpha = BlendData[mode].SrcBlendAlpha;
	BlendDesc.RenderTarget[0].DestBlendAlpha = BlendData[mode].DestBlendAlpha;
	BlendDesc.RenderTarget[0].BlendOpAlpha = BlendData[mode].BlendOpAlpha;
	BlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	hr = p_Device->CreateBlendState(&BlendDesc, &BlendState[mode]);
	if (FAILED(hr))return false;
    }
    bLoad = true;
    enumMode = MODE_MAX;
    return true;
}